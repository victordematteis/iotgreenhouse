import React, {Component} from 'react';
import { Line } from 'react-chartjs-2';
import openSocket from 'socket.io-client';
import 'chartjs-plugin-datalabels';
import axios from 'axios';

var dataCostantCreator0;/*<--*/
var dataCostantCreator1;
var dataCostantCreator2;
var indicator0;

var sock = openSocket('http://scotchnetpi.zapto.org:3002');


class App extends Component{
  
  componentDidMount() {
    sock.on('graphData' + /*-->*/ 'tty-irrigazione' + /*-->*/'0', sms =>{
      sms = sms.toString();
      console.log("TO STRINGA = " + sms);
      var messageArrayGlobal = sms.split(",");
      /*-->*/ dataCostantCreator0 = {
        labels: ['1', '2', '3', '4', '5','1', '2', '3', '4', '5','1', '2', '3', '4', '5','1', '2', '3', '4', '5','1', '2', '3', '4', '5',],
        datasets: [
          {
            label: 'My First dataset',
            fill: false,
            lineTension: 0.5,
            backgroundColor: 'rgba(75,192,192,0.4)',
            borderColor: 'rgba(75,192,192,1)',
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            pointBorderColor: 'rgba(75,192,192,1)',
            pointBackgroundColor: '#fff',
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: 'rgba(75,192,192,1)',
            pointHoverBorderColor: 'rgba(220,220,220,1)',
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: messageArrayGlobal
          }
        ]
      };
      this.forceUpdate();
    });
    sock.on('graphData' + /*-->*/ 'tty-irrigazione' + /*-->*/'3', sms =>{
      sms = sms.toString();
      console.log("TO STRINGA = " + sms);
      var messageArrayGlobal = sms.split(",");
      indicator0 = messageArrayGlobal[(messageArrayGlobal.length)-1];
      this.forceUpdate();
    });
  }
  constructor(props){
    super(props);
    this.state = {
      data: '',
      isLoading: false,
    };
    //@this.link@
    this.bottoneOn = this.bottoneOn.bind(this);
    this.bottoneOff = this.bottoneOff.bind(this);
  }
  //@function@
  bottoneOn() {
    axios.post("http://scotchnetpi.zapto.org:3002/" + "turnOn");
  }
  bottoneOff() {
    axios.post("http://scotchnetpi.zapto.org:3002/" + "turnOff");
  }
  render() {
      return (
        <div>
          <h2>Sensore tty-irrigazione 0</h2>
          <Line ref="chart" data={dataCostantCreator0} />
          {/*Element*/}
          <h2>Indicatore temperatura = {indicator0} </h2>
          {/*Button*/}
          <button className="postButtonInserted" onClick={this.bottoneOn}> bottoneOn </button>
          <button className="postButtonInserted" onClick={this.bottoneOff}> bottoneOff </button>
        </div>
      );
  };
}
export default App;

//LINK UTILE: https://stackoverflow.com/questions/49445911/how-to-get-axios-post-info-after-click-a-button-once