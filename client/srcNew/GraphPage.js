import React, {Component} from 'react';
import { Line } from 'react-chartjs-2';
import openSocket from 'socket.io-client';
import 'chartjs-plugin-datalabels';
import axios from 'axios';
import './css/default.css';

var graph0;
var graph1;
var indicator2
var indicator3
//@variableNameList@


var sock = openSocket('http://scotchnetpi.zapto.org:3002');
class App extends Component{
  componentDidMount() {


sock.on('graphData' + /*-->*/ 'tty-dati1' + /*-->*/'0', sms =>{
      sms = sms.toString();
      console.log("TO STRINGA = " + sms);
      var messageArrayGlobal = sms.split(",");
      /*-->*/ graph0 = {
        labels: ['1', '2', '3', '4', '5','1', '2', '3', '4', '5','1', '2', '3', '4', '5','1', '2', '3', '4', '5','1', '2', '3', '4', '5',],
        datasets: [
          {
            label: 'Dati ricevuti',
            fill: false,
            lineTension: 0.5,
            backgroundColor: 'rgba(75,192,192,0.4)',
            borderColor: 'rgba(75,192,192,1)',
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            pointBorderColor: 'rgba(75,192,192,1)',
            pointBackgroundColor: '#fff',
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: 'rgba(75,192,192,1)',
            pointHoverBorderColor: 'rgba(220,220,220,1)',
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: messageArrayGlobal
          }
        ]
      };
      this.forceUpdate();
    });
sock.on('graphData' + /*-->*/ 'tty-braccio' + /*-->*/'0', sms =>{
      sms = sms.toString();
      console.log("TO STRINGA = " + sms);
      var messageArrayGlobal = sms.split(",");
      /*-->*/ graph1 = {
        labels: ['1', '2', '3', '4', '5','1', '2', '3', '4', '5','1', '2', '3', '4', '5','1', '2', '3', '4', '5','1', '2', '3', '4', '5',],
        datasets: [
          {
            label: 'Dati ricevuti',
            fill: false,
            lineTension: 0.5,
            backgroundColor: 'rgba(75,192,192,0.4)',
            borderColor: 'rgba(75,192,192,1)',
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            pointBorderColor: 'rgba(75,192,192,1)',
            pointBackgroundColor: '#fff',
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: 'rgba(75,192,192,1)',
            pointHoverBorderColor: 'rgba(220,220,220,1)',
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: messageArrayGlobal
          }
        ]
      };
      this.forceUpdate();
    });
sock.on('graphData' + /*-->*/ 'tty-allarme' + /*-->*/'1', sms =>{
      sms = sms.toString();
      console.log("TO STRINGA = " + sms);
      var messageArrayGlobal = sms.split(",");
      indicator2 = messageArrayGlobal[(messageArrayGlobal.length)-1];
      this.forceUpdate();
    });sock.on('graphData' + /*-->*/ 'tty-braccio' + /*-->*/'0', sms =>{
      sms = sms.toString();
      console.log("TO STRINGA = " + sms);
      var messageArrayGlobal = sms.split(",");
      indicator3 = messageArrayGlobal[(messageArrayGlobal.length)-1];
      this.forceUpdate();
    });}
  constructor(props){
    super(props);
    this.state = {
      data: '',
      isLoading: false,
    };
    this.muoviMotori = this.muoviMotori.bind(this);
this.Cambia_temperatura_a_35 = this.Cambia_temperatura_a_35.bind(this);
this.spegniVentola = this.spegniVentola.bind(this);
this.attivaAllarme = this.attivaAllarme.bind(this);
this.disattivaAllarme = this.disattivaAllarme.bind(this);
this.attivaFuoco = this.attivaFuoco.bind(this);
this.disattivaFuoco = this.disattivaFuoco.bind(this);
this.accendiVentolaBraccio = this.accendiVentolaBraccio.bind(this);
//@this.link@
  }
  muoviMotori() {
    axios.post("http://scotchnetpi.zapto.org:3002/" + "muoviMotori");
  }
Cambia_temperatura_a_35() {
    axios.post("http://scotchnetpi.zapto.org:3002/" + "Cambia_temperatura_a_35");
  }
spegniVentola() {
    axios.post("http://scotchnetpi.zapto.org:3002/" + "spegniVentola");
  }
attivaAllarme() {
    axios.post("http://scotchnetpi.zapto.org:3002/" + "attivaAllarme");
  }
disattivaAllarme() {
    axios.post("http://scotchnetpi.zapto.org:3002/" + "disattivaAllarme");
  }
attivaFuoco() {
    axios.post("http://scotchnetpi.zapto.org:3002/" + "attivaFuoco");
  }
disattivaFuoco() {
    axios.post("http://scotchnetpi.zapto.org:3002/" + "disattivaFuoco");
  }
accendiVentolaBraccio() {
    axios.post("http://scotchnetpi.zapto.org:3002/" + "accendiVentolaBraccio");
  }
//@function@
  render() {
      return (
        <div>
          <h2 class="labelGraphgraph0">Sensore ARDdati temperatura</h2>
<Line class="graphClassgraph0" ref="chart" data={graph0} />
<h2 class="labelGraphgraph1">Sensore ARDbraccio tempMotori</h2>
<Line class="graphClassgraph1" ref="chart" data={graph1} />
<h2 class="indicatorClassindicator2">Sensore ARDallarme fuoco: {indicator2} </h2>
<h2 class="indicatorClassindicator3">Sensore ARDbraccio tempMotori: {indicator3} </h2>
{/*Element*/}
	<div class="bottoneria">
          <button className="postButtonInserted" onClick={this.muoviMotori}> muoviMotori </button>
<button className="postButtonInserted" onClick={this.Cambia_temperatura_a_35}> Cambia_temperatura_a_35 </button>
<button className="postButtonInserted" onClick={this.spegniVentola}> spegniVentola </button>
<button className="postButtonInserted" onClick={this.attivaAllarme}> attivaAllarme </button>
<button className="postButtonInserted" onClick={this.disattivaAllarme}> disattivaAllarme </button>
<button className="postButtonInserted" onClick={this.attivaFuoco}> attivaFuoco </button>
<button className="postButtonInserted" onClick={this.disattivaFuoco}> disattivaFuoco </button>
<button className="postButtonInserted" onClick={this.accendiVentolaBraccio}> accendiVentolaBraccio </button>
{/*Button*/}
	</div>
        </div>
      );
  };
}
export default App;

//LINK UTILE: https://stackoverflow.com/questions/49445911/how-to-get-axios-post-info-after-click-a-button-once