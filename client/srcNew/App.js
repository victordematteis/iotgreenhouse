import React, { Component } from 'react';
import{
  BrowserRouter as Router,
  Route,
  Link
} from 'react-router-dom';

//components
import Header from './Components/headerComponent/header'; //he already knows, the ext. is not necessary.
import Footer from './Components/footerComponent/footer';
import Toolbar from './Components/toolBar/Toolbar.js';
import Sidebar from './Components/sideBar/sideBar';
import SideDrawer from './Components/sideDrawer/SideDrawer.js';

//Pages
import Homepage from './Components/pages/homePage';
import Products from './Components/pages/products'
import Graph from './GraphPage.js';

//include
import './Assets/css/pageCss.css';
//

class App extends Component{
  render(){
    return (
      <Router>
        <div className="App">
          <Toolbar />
              <div className="pageContent">
                <Route exact path='/' component={Homepage} />
                <Route exact path='/Products' component={Products} />
                <Route exact path='/Graph' component={Graph} />
              </div>
          <Footer />
        </div>
      </Router>
    );
  }
}

export default App;
//