import React, { Component } from 'react';
import{
    Link
  } from 'react-router-dom';
import './Toolbar.css';
import  CoolButton from '../sideDrawer/DrawerToggleButton.js';

const toolbar = props => (
    <header className="toolbar">
        <nav className="toolbar__navigation">
            <div>
                <CoolButton />
            </div>
            <div className="toolbar__logo"><a href="/">LOGO</a></div>
            <div className="spacer" />
            <div className="toolbar_navigation-items">
                <ul>
                    <li><Link to="/">Home</Link></li>
                    <li><Link to="/Graph">Sensors</Link></li>
                </ul>
            </div>
        </nav>
    </header>
);


export default toolbar;