import React, {Component} from 'react';
import { Line } from 'react-chartjs-2';
import openSocket from 'socket.io-client';
import 'chartjs-plugin-datalabels';
import axios from 'axios';
import './css/default.css';

var graph0;
var graph1;
var graph2;
//@variableNameList@


var optionsVariable = {
  scales: {
    yAxes: [{
      ticks: {
        beginAtZero: true,
        min: 0,
        max: 200
      }    
    }]
  }
};

var sock = openSocket('http://192.168.0.106:3002');
class App extends Component{
  componentDidMount() {


sock.on('graphData' + /*-->*/ 'tty-dati1' + /*-->*/'0', sms =>{
      sms = sms.toString();
      console.log("TO STRINGA = " + sms);
      var messageArrayGlobal = sms.split(",");
      /*-->*/ graph0 = {
        labels: ['1', '2', '3', '4', '5','1', '2', '3', '4', '5','1', '2', '3', '4', '5','1', '2', '3', '4', '5','1', '2', '3', '4', '5',],
        datasets: [
          {
            label: 'Dati ricevuti',
            fill: false,
            lineTension: 0.5,
            backgroundColor: 'rgba(75,192,192,0.4)',
            borderColor: 'rgba(75,192,192,1)',
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            pointBorderColor: 'rgba(75,192,192,1)',
            pointBackgroundColor: '#fff',
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: 'rgba(75,192,192,1)',
            pointHoverBorderColor: 'rgba(220,220,220,1)',
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: messageArrayGlobal
          }
        ]
      };
      this.forceUpdate();
    });
sock.on('graphData' + /*-->*/ 'tty-dati1' + /*-->*/'1', sms =>{
      sms = sms.toString();
      console.log("TO STRINGA = " + sms);
      var messageArrayGlobal = sms.split(",");
      /*-->*/ graph1 = {
        labels: ['1', '2', '3', '4', '5','1', '2', '3', '4', '5','1', '2', '3', '4', '5','1', '2', '3', '4', '5','1', '2', '3', '4', '5',],
        datasets: [
          {
            label: 'Dati ricevuti',
            fill: false,
            lineTension: 0.5,
            backgroundColor: 'rgba(75,192,192,0.4)',
            borderColor: 'rgba(75,192,192,1)',
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            pointBorderColor: 'rgba(75,192,192,1)',
            pointBackgroundColor: '#fff',
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: 'rgba(75,192,192,1)',
            pointHoverBorderColor: 'rgba(220,220,220,1)',
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: messageArrayGlobal
          }
        ]
      };
      this.forceUpdate();
    });
sock.on('graphData' + /*-->*/ 'tty-dati1' + /*-->*/'2', sms =>{
      sms = sms.toString();
      console.log("TO STRINGA = " + sms);
      var messageArrayGlobal = sms.split(",");
      /*-->*/ graph2 = {
        labels: ['1', '2', '3', '4', '5','1', '2', '3', '4', '5','1', '2', '3', '4', '5','1', '2', '3', '4', '5','1', '2', '3', '4', '5',],
        datasets: [
          {
            label: 'Dati ricevuti',
            fill: false,
            lineTension: 0.5,
            backgroundColor: 'rgba(75,192,192,0.4)',
            borderColor: 'rgba(75,192,192,1)',
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            pointBorderColor: 'rgba(75,192,192,1)',
            pointBackgroundColor: '#fff',
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: 'rgba(75,192,192,1)',
            pointHoverBorderColor: 'rgba(220,220,220,1)',
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: messageArrayGlobal
          }
        ]
      };
      this.forceUpdate();
    });
}
  constructor(props){
    super(props);
    this.state = {
      data: '',
      isLoading: false,
    };
    this.cambiaDati1 = this.cambiaDati1.bind(this);
this.mostraDati = this.mostraDati.bind(this);
//@this.link@
  }
  cambiaDati1() {
    axios.post("http://192.168.0.106:3002/" + "cambiaDati1");
  }
mostraDati() {
    axios.post("http://192.168.0.106:3002/" + "mostraDati");
  }
//@function@
  render() {
      return (
        <div>
                    <h2 className="labelGraphgraph0">Sensore ARDdati umidità1</h2>
          <Line className="graphClassgraph0" ref="chart" options={optionsVariable} data={graph0} />
          <h2 className="labelGraphgraph1">Sensore ARDdati umidità2</h2>
          <Line className="graphClassgraph1" ref="chart" options={optionsVariable} data={graph1} />
          <h2 className="labelGraphgraph2">Sensore ARDdati umidità3</h2>
          <Line className="graphClassgraph2" ref="chart" options={optionsVariable} data={graph2} />
{/*Element*/}
	<div class="bottoneria">
          <button className="postButtonInserted" onClick={this.cambiaDati1}> cambiaDati1 </button>
<button className="postButtonInserted" onClick={this.mostraDati}> mostraDati </button>
{/*Button*/}
	</div>
        </div>
      );
  };
}
export default App;

//LINK UTILE: https://stackoverflow.com/questions/49445911/how-to-get-axios-post-info-after-click-a-button-once