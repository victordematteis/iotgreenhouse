import React, { Component } from 'react';
import{
    Link
  } from 'react-router-dom';

import "./SideDrawer.css";

const sideDrawer = props => (
    <nav className="side-drawer">
        <ul>
            <li><Link to="/">Home</Link> </li>
            <li><Link to="/Graph">Sensors</Link> </li>
        </ul>
    </nav>
);
export default sideDrawer;
