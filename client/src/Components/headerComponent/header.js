import React, { Component } from 'react';
import{
    Link
  } from 'react-router-dom';


//hello
class Header extends Component {
  render(){
    return (
        <header>
            <div className="logo">
                LOGO 'n' stuff
            </div>
            <nav className="links">
                <ul>
                    <li>
                        <Link to="/">Home</Link> 
                    </li>
                    <li>
                        <Link to="/Products">Products</Link> 
                    </li>
                </ul>
            </nav>
        </header>
    );
  }
}

export default Header;