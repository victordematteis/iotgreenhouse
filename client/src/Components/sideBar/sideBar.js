import React, { Component } from 'react';
import{
    Link
  } from 'react-router-dom';


//hello
class Sidebar extends Component {
  render(){
    return (
        <div className="sideBar">
            <div className="Biglogo">
                IoT GreenHouse
            </div>
            <div className="MediumLogo"> 
                IoT
            </div>
            <nav className="links">
                <ul>
                    <li>
                        <Link to="/">Home</Link> 
                    </li>
                    <li>
                        <Link to="/Products">Products</Link> 
                    </li>
                    <li>
                        <Link to="/Graph">View Data</Link> 
                    </li>
                </ul>
            </nav>
        </div>
    );
  }
}

export default Sidebar;