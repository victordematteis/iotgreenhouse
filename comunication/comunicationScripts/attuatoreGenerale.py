#!/usr/bin/python
import serial
import time
import os
import sys
from serial import Serial
import string

lettura = ""

# attuatoreGenerale.py [1: device name] [2: value to change] [3: new value] [4: time to wait] 
# [5: '-start' create void instructions document] 
# [5: '-set' write on file instructions]
# [6: list of instructions]

mainInstFile = "./comunication/comunicationScripts/instr." + sys.argv[1] + ".txt"
fp = ""

exists = os.path.isfile(mainInstFile)
if exists:
    print("OK")
else:
    print("############## !!! ###########")
    fp = open(mainInstFile, "wx")
    for countA in range(0, int(sys.argv[6])):
        print("Creted line!")
        fp.write("0\n")
    fp.close()



if len(sys.argv) > 5:
    if sys.argv[5] == '-start':
        print("############## !!! ###########")
        fp = open(mainInstFile, "w+")
        for countA in range(0, int(sys.argv[6])):
            print("Creted line!")
            fp.write("0\n")
        fp.close()
        exit()
    if sys.argv[5] == '-set':
        print("SETTING")
        fp = open(mainInstFile,'r')
        lettura = fp.read()
        print("LETTURA FILE PRIMA DELLA MODIFICA:\n" + lettura)
        lettura = lettura.split('\n')
        lettura[int(sys.argv[2])] = str(sys.argv[3])
        lettura = string.join(lettura, '\n')
        fp.close
        fp = open(mainInstFile,'w+')
        fp.write(lettura)
        fp.close
        print("AFTER:" + lettura + "\nNon verranno inviati comandi")
        exit()

ser = serial.Serial('/dev/' + sys.argv[1], 9600)
print('Connected to: ', sys.argv[1])

fp = open(mainInstFile,'r')
lettura = fp.read()
print("LETTURA FILE PRIMA DELLA MODIFICA:\n" + lettura)
lettura = lettura.split('\n')
lettura[int(sys.argv[2])] = str(sys.argv[3])
lettura = string.join(lettura, '\n')
fp.close
fp = open(mainInstFile,'w+')
fp.write(lettura)
fp.close
fp = open(mainInstFile,'r')


while ser.in_waiting: 
    ser.read()

print('connected.')
print('waiting')
time.sleep(int(sys.argv[4]))
print('wait completed.')

print('MESSAGE THAT WILL BE SENT:')
print(lettura)
ser.write(lettura)
fp.close()
print('operazione temrinata ')

