#!/usr/bin/python
import serial
from serial import Serial
import time
import os
import sys
 


#letturaDati.py [1: device name] [2: number of sensors attached]    [3: '-act' if instructions have to be performed] [4: '-start' to create void file of instructions.txt] [5: number of instructions] 
#To leave emp
# attuatoreGenerale.py [1: device name] [2: value to change] [3: new value] [4: time to wait]

ser = serial.Serial('/dev/' + sys.argv[1], 9600)

connected = False
ser.close()
ser.open()
if len(sys.argv) > 4:
    if sys.argv[4] == '-start':
        fp = open("./comunication/comunicationScripts/results/" + str() + sys.argv[1] + ".txt", "w+")
        for countA in range(0, int(sys.argv[5])):
            fp.write("0\n")
        fp.close()
while 1:
    stringaTotale = ""
    while not connected:
        serin = ser.read()
        connected = True
    stringaRisultato = ""
    x=ser.read()
    while x is not '<':
        x=ser.read()
    for countB in range(0,int(sys.argv[2])):
        stringaRisultato = ""
        while 1:
            if ser.in_waiting:
                x=ser.read()
                if x=="|" or x==">":
                    break;
                stringaRisultato = stringaRisultato + x 
        stringaRisultato = stringaRisultato.replace('<','')         
        stringaTotale += stringaRisultato + "/";
        fp = open('./comunication/comunicationScripts/results/'+ str(countB) + sys.argv[1] + '.txt','w+')
        fp.write(stringaRisultato)
        fp.close()
    if  len(sys.argv) > 3:
        if sys.argv[3] == "-act":
            fileInst = open("./comunication/comunicationScripts/instr." + sys.argv[1] + '.txt', 'r')
            lettura = fp.read()
            ser.write(lettura)
            fileInst.close()

    sys.stdout.write(stringaTotale)
    print(stringaTotale)
    sys.stdout.flush()