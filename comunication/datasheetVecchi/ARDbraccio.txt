DEVICENAME: tty-braccio
SENSORS: 1
DATA_SENT: 7
WAIT_TIME_SEC: 6
ACTUATORS: 3

SENSORI READ:
0 tempMotori

ATTUATORI WRITE:
---------------------------
0 braccioRobotico
> attiva = 1
> normale = 0 (non dare comando)
----------------------------------
1 boolTemperatura
> attiva = 1
> disattiva = 0
---------------------------------
2 valTemperatura
> valore = @d
