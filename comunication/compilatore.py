import sys
import os
import shutil
from shutil import copyfile
from os import walk

lettura = ""
indicator = 0







def compilazioneGrafico(arrayLinea, nGraf, indicator):
    #serverSide
    fileDatasheet = open('./datasheet/' + arrayLinea[1] + '.txt','r')
    bufferLettura = fileDatasheet.readline()
    bufferLettura = bufferLettura.split(' ')
    nomeDispositivo = bufferLettura[1]
    nomeDispositivo = nomeDispositivo.replace('\n','')
    bufferLettura = fileDatasheet.readline()
    bufferLettura = bufferLettura.split(' ')
    numeroSensori = bufferLettura[1]
    numeroSensori = numeroSensori.replace('\n','')
    bufferLettura = fileDatasheet.readline()
    bufferLettura = fileDatasheet.readline()
    numeroSpecifico = ""
    nomeSensore = arrayLinea[3]
    for line in fileDatasheet:
        line = line.replace('\n','')
        line = line.split(' ')
        if line.__len__() > 1:
            print('arrayLinea[3] = ' + arrayLinea[3] + '  - linea[1] = '+ line[1])
            if str(line[1]) == str(arrayLinea[3]):
                print('entrato')
                numeroSpecifico = line[0]
                numeroSpecifico = numeroSpecifico.replace('\n','') 
        if line[0] == "ATTUATORI":
            break
    fileRisultato = open('./resultScript/compiledGraphFiles/serverModel/RESULT.txt', 'a+')
    fileGraph = open('./resultScript/compiledGraphFiles/serverModel/graph.txt','r')
    lettura = fileGraph.read()
    lettura = str(lettura)
    lettura = lettura.replace('@deviceName@', nomeDispositivo)
    lettura = lettura.replace('@sensorsNumber@', numeroSensori)
    lettura = lettura.replace('@number@', str(nGraf))
    print(str(nGraf))
    print("NUMERO SPECIFICO = " + numeroSpecifico)
    lettura = lettura.replace('@specificSensorNumber@', numeroSpecifico)
    fileRisultato.write(lettura)
    fileRisultato.close()   

    if indicator is 0:
        fileGuida = open('./resultScript/compiledGraphFiles/clientModel/base.txt', 'r')
        fileRisultato = open('./resultScript/compiledGraphFiles/clientModel/RESULT.txt', 'r')
        letturaRis = fileRisultato.read()
        fileRisultato = open('./resultScript/compiledGraphFiles/clientModel/RESULT.txt', 'w')
        fileRisultato.write(letturaRis.replace('//@variableNameList@', 'var graph' + str(nGraf) + ';\n//@variableNameList@'))
        fileRisultato.close()
        fileRisultato = open('./resultScript/compiledGraphFiles/clientModel/RESULT.txt', 'a')
        fileGraph = open('./resultScript/compiledGraphFiles/clientModel/graph.txt','r')
        lettura = fileGraph.read()
        lettura = lettura.replace('@deviceName@', nomeDispositivo)
        lettura = lettura.replace('@specificSensorNumber@', numeroSpecifico)
        lettura = lettura.replace('@variableName@', 'graph' + str(nGraf))
        fileRisultato.write(lettura)
        fileRisultato.close()
        ftailGraphBase = open('./resultScript/compiledGraphFiles/clientModel/graphTail.txt', 'r')
        lettura = ftailGraphBase.read()
        lettura = lettura.replace('@deviceName@', arrayLinea[1])
        lettura = lettura.replace('@specificSensorNumber@', arrayLinea[3])
        tagsText = lettura.replace('@variableNameList@', 'graph' + str(nGraf))
        ftailBase = open('./resultScript/compiledGraphFiles/clientModel/tailRESULT.txt', 'r')
        tailResultText = ftailBase.read()
        tailResultText = tailResultText.replace('{/*Element*/}', tagsText + "\n{/*Element*/}")
        ftailBase.close()
        resultFile = open('./resultScript/compiledGraphFiles/clientModel/tailRESULT.txt', 'w+')
        resultFile.write(tailResultText)
        resultFile.close()

    else:
        fileGuida = open('./resultScript/compiledGraphFiles/clientModel/base.txt', 'r')
        fileRisultato = open('./resultScript/compiledGraphFiles/clientModel/RESULT.txt', 'r')
        letturaRis = fileRisultato.read()
        fileRisultato = open('./resultScript/compiledGraphFiles/clientModel/RESULT.txt', 'w')
        fileRisultato.write(letturaRis.replace('//@variableNameList@', 'var indicator' + str(nGraf) + '\n//@variableNameList@'))
        fileRisultato = open('./resultScript/compiledGraphFiles/clientModel/RESULT.txt', 'a')
        fileGraph = open('./resultScript/compiledGraphFiles/clientModel/indicator.txt','r')
        lettura = fileGraph.read()
        lettura = lettura.replace('@deviceName@', nomeDispositivo)
        lettura = lettura.replace('@specificSensorNumber@', numeroSpecifico)
        lettura = lettura.replace('@variableName@', 'indicator' + str(nGraf))
        fileRisultato.write(lettura)
        fileRisultato.close()
        ftailGraphBase = open('./resultScript/compiledGraphFiles/clientModel/indicatorTail.txt', 'r')
        lettura = ftailGraphBase.read()
        lettura = lettura.replace('@deviceName@', arrayLinea[1])
        lettura = lettura.replace('@specificSensorNumber@', arrayLinea[3])
        tagsText = lettura.replace('@variableName@', 'indicator' + str(nGraf))
        ftailBase = open('./resultScript/compiledGraphFiles/clientModel/tailRESULT.txt', 'r')
        tailResultText = ftailBase.read()
        tailResultText = tailResultText.replace('{/*Element*/}', tagsText + "\n{/*Element*/}")
        ftailBase.close()
        resultFile = open('./resultScript/compiledGraphFiles/clientModel/tailRESULT.txt', 'w+')
        resultFile.write(tailResultText)
        resultFile.close()


def startupAttuatore(actuators, deviceName):
    mainInstFile = "./comunicationScripts/instr." + deviceName + ".txt"
    print("############## !!! ###########")
    fp = open(mainInstFile, 'w+')
    for countA in range(0, int(actuators)):
        print("Creted line!")
        fp.write("0\n")
    fp.close()


def scriviAzione(comandsFile, onlyActions):
    specificSensorNumber = ""
    deviceName = ""
    variableName = ""
    condition = ""
    #parte della consegunenza
    deviceName = ""
    valueToChange = ""
    newValue = ""
    waitTime = ""
    actuators = ""
    dynamicVariable = 0
    for lines in comandsFile:
        print("LINEA = " + lines)
        print("entrato in lines comandfile")
        lines = lines.replace('\n','')
        lines = lines.split(" ")
        if lines[0] != "==>(" and lines[0] != "==>[":
            print("uscitov da lines comand file, perche lines[0]:<" + lines[0] + ">")
            break
        else:
            #ottengo le istruzioni per l'azione
            fileDatasheet = open('./datasheet/' + lines[1] + '.txt', 'r')
            for linesDatasheet in fileDatasheet:
                linesDatasheet = linesDatasheet.replace('\n','')
                linesDatasheet = linesDatasheet.split(" ")
                if linesDatasheet[0] == "DEVICENAME:":
                    deviceName = linesDatasheet[1]
                if linesDatasheet[0] == 'WAIT_TIME_SEC:':
                    waitTime = linesDatasheet[1]
                if linesDatasheet[0] == 'ACTUATORS:':
                    actuators = linesDatasheet[1] 
                if linesDatasheet.__len__() > 1:
                    if valueToChange is not '' and linesDatasheet[0] is '>':
                        if linesDatasheet[1] == lines[3]:
                            newValue = linesDatasheet[3]
                            if newValue == '@v':
                                fileVariabili = open('./variabili.txt', 'r')
                                for lineVar in fileVariabili:
                                    lineVar = lineVar.replace('\n', '')
                                    lineVar = lineVar.split(",")
                                    if lineVar[0] == lines[4]:
                                        newValue = lineVar[1]
                            if newValue == '@d':
                                dynamicVariable = 1
                    if linesDatasheet[0] is not '>' and linesDatasheet[1] == lines[2]:
                        valueToChange = linesDatasheet[0]
            if lines[0] == "==>[" and onlyActions is 1:
                fileGuidaAzione = open('./resultScript/convertedScripts/setBaseNoIndent.txt', 'r')
                print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Inserted beause lines[0] = "  + lines[0] + " onlyActions = " + str(onlyActions))
            elif lines[0] == "==>[":
                fileGuidaAzione = open('./resultScript/convertedScripts/setBase.txt', 'r')
                print(">>>>>>>>>>>>>>>>>>>> Inserted beause lines[0] = "  + lines[0])
            elif dynamicVariable is 1 and onlyActions is 1:
                fileGuidaAzione = open('./resultScript/convertedScripts/dynamicBaseNoIndent.txt', 'r')
            elif dynamicVariable is 1:
                fileGuidaAzione = open('./resultScript/convertedScripts/dynamicBase.txt', 'r')
            elif onlyActions is 1:
                fileGuidaAzione = open('./resultScript/convertedScripts/actionBaseNoIndent.txt', 'r')
            else:
                fileGuidaAzione = open('./resultScript/convertedScripts/actionBase.txt', 'r')
            actionBaseBuffer = fileGuidaAzione.read()
            fileRisultato = open('./resultScript/convertedScripts/RESULTS/' + scriptName + '-' + str(nScript) + '-B' + '.py', 'a')
            if onlyActions is 1:
                actionBaseBuffer = actionBaseBuffer.replace('    ','',1)
            actionBaseBuffer = actionBaseBuffer.replace('@deviceName@', deviceName)
            actionBaseBuffer = actionBaseBuffer.replace('@valueToChange@', valueToChange)
            actionBaseBuffer = actionBaseBuffer.replace('@newValue@', newValue)
            actionBaseBuffer = actionBaseBuffer.replace('@waitTime@', waitTime)
            actionBaseBuffer = actionBaseBuffer.replace('@actuators@', actuators)
            actionBaseBuffer = actionBaseBuffer.replace('@variableName@', lines[4])
            fileRisultato.write('\n' + actionBaseBuffer )           
            fileRisultato.close()
            startupAttuatore(actuators, deviceName)




def compilatoreScript(lineCondition, comandsFile, scriptName, secondiRipetizione, nScript):
    fileDatasheet = open('./datasheet/' + line[1] + '.txt', 'r')
    fileGuidaAzione = open('./resultScript/convertedScripts/actionBase.txt', 'r')
    fileGuida = open('./resultScript/convertedScripts/base.txt', 'r')      
    fileRisultato = open('./resultScript/convertedScripts/RESULTS/' + scriptName + '-' + str(nScript) + '-' + secondiRipetizione + '.py', 'w+')
    #parte dell'if
    specificSensorNumber = ""
    deviceName = ""
    variableName = ""
    condition = ""
    #parte della consegunenza
    deviceName = ""
    valueToChange = ""
    newValue = ""
    waitTime = ""
    actuators = ""
    #fetching di queste informazioni [[IF]]
    for lines in fileDatasheet:
        lines = lines.replace('\n','')
        lines = lines.split(" ")
        if lines[0] == "DEVICENAME:":
            deviceName = lines[1]
        if lines[0] == 'WAIT_TIME_SEC:':
            waitTime = lines[1]
        if lines[0] == 'ACTUATORS:':
            actuators = lines[1]
        if lines[0] == 'ATTUATORI':
            break
        if lines[0] != '':
            if lines[1] == lineCondition[3]:
                specificSensorNumber = lines[0]
    fileGuida = open('./resultScript/convertedScripts/base.txt', 'r')  
    conditionScript = lineCondition[4]
    if conditionScript is '=':
        conditionScript = 'is'          
    fileGuidaSas = fileGuida.read()
    fileGuidaSas = str(fileGuidaSas)
    fileGuidaSas = fileGuidaSas.replace('@specificSensorNumber@',specificSensorNumber)
    fileGuidaSas = fileGuidaSas.replace('@deviceName@', deviceName)
    fileGuidaSas = fileGuidaSas.replace('@variableName@', lineCondition[5])
    fileGuidaSas = fileGuidaSas.replace('@condition@', conditionScript)
    fileRisultato.write(fileGuidaSas + '\n')
    fileRisultato.close()
    fileRisultato = open('./resultScript/convertedScripts/RESULTS/' + scriptName + '-' + str(nScript) + '-' + secondiRipetizione + '.py', 'a')
    scriviAzione(comandsFile,0)
            

    

def compilatoreBottone(comandsFile, scriptName, nScript):
    fileGuida = open('./resultScript/convertedScripts/base.txt', 'r')      
    lineaincl = fileGuida.readline()
    fileGuida.close()
    fileGuidaAzione = open('./resultScript/convertedScripts/actionBase.txt', 'r')  
    fileRisultato = open('./resultScript/convertedScripts/RESULTS/' + scriptName + '-' + str(nScript) + '-B' + '.py', 'w+')   
    #parte della consegunenza
    deviceName = ""
    valueToChange = ""
    newValue = ""
    waitTime = ""
    actuators = ""
    dynamicVariable = 0
    #fetching di queste informazioni [[IF]]
    fileRisultato = open('./resultScript/convertedScripts/RESULTS/' + scriptName + '-' + str(nScript) + '-B' + '.py', 'a')
    fileRisultato.write(lineaincl)
    fileRisultato.close()
    scriviAzione(comandsFile,1)
    #serverside
    fileServer = open('./resultScript/compiledGraphFiles/serverModel/button.txt', 'r')
    txtResult = fileServer.read()
    txtResult = txtResult.replace('@functionName@', scriptName)
    txtResult = txtResult.replace('@number@', str(nScript))
    fileResult = open('./resultScript/compiledGraphFiles/serverModel/RESULT.txt', 'a+')
    fileResult.write(txtResult)
    #client side
    buttonFile = open('./resultScript/compiledGraphFiles/clientModel/button/button.txt', 'r')
    buttonText = buttonFile.read()
    buttonText = buttonText.replace('@functionName@', scriptName)
    functionFile = open('./resultScript/compiledGraphFiles/clientModel/button/function.txt', 'r')
    functionText = functionFile.read()
    functionText = functionText.replace('@functionName@', scriptName)
    linkFile = open('./resultScript/compiledGraphFiles/clientModel/button/link.txt', 'r')
    linkText = linkFile.read()
    linkText = linkText.replace('@functionName@', scriptName)
    tailResFile = open('./resultScript/compiledGraphFiles/clientModel/tailRESULT.txt', 'r')
    tailResText = tailResFile.read()
    tailResText = tailResText.replace('//@this.link@', linkText + '\n//@this.link@')
    tailResText = tailResText.replace('//@function@', functionText + '\n//@function@')
    tailResText = tailResText.replace('{/*Button*/}', buttonText +'\n{/*Button*/}')
    tailResFile.close()
    tailResFile = open('./resultScript/compiledGraphFiles/clientModel/tailRESULT.txt', 'w')
    tailResFile.write(tailResText)
    tailResFile.close()
 

def compilatoreBottoneCondizione(lineCondition, comandsFile, scriptName, nScript):
    fileDatasheet = open('./datasheet/' + lineCondition[1] + '.txt', 'r')
    fileGuidaAzione = open('./resultScript/convertedScripts/actionBase.txt', 'r')
    fileGuida = open('./resultScript/convertedScripts/base.txt', 'r')      
    fileRisultato = open('./resultScript/convertedScripts/RESULTS/' + scriptName + '-'  + str(nScript) + '-' + 'B' + '.py', 'w+')
    #parte dell'if
    specificSensorNumber = ""
    deviceName = ""
    variableName = ""
    condition = ""
    #parte della consegunenza
    deviceName = ""
    valueToChange = ""
    newValue = ""
    waitTime = ""
    actuators = ""
    dynamicVariable = 0
    #fetching di queste informazioni [[IF]]
    for lines in fileDatasheet:
        lines = lines.replace('\n','')
        lines = lines.split(" ")
        if lines[0] == "DEVICENAME:":
            deviceName = lines[1]
        if lines[0] == 'WAIT_TIME_SEC:':
            waitTime = lines[1]
        if lines[0] == 'ATTUATORI':
            break
        if lines[0] != '':
            if lines[1] == lineCondition[3]:
                specificSensorNumber = lines[0]
    conditionScript = lineCondition[4]
    if conditionScript is '=':
        conditionScript = 'is'
    fileGuida = open('./resultScript/convertedScripts/base.txt', 'r')            
    fileGuidaSas = fileGuida.read()
    fileGuidaSas = str(fileGuidaSas)
    fileGuidaSas = fileGuidaSas.replace('@specificSensorNumber@',specificSensorNumber)
    fileGuidaSas = fileGuidaSas.replace('@deviceName@', deviceName)
    fileGuidaSas = fileGuidaSas.replace('@variableName@', lineCondition[5])
    fileGuidaSas = fileGuidaSas.replace('@condition@', conditionScript)
    fileRisultato.write(fileGuidaSas)
    fileRisultato.close()
    scriviAzione(comandsFile,0)
    #serverside
    fileServer = open('./resultScript/compiledGraphFiles/serverModel/button.txt', 'r')
    txtResult = fileServer.read()
    txtResult = txtResult.replace('@functionName@', scriptName)
    txtResult = txtResult.replace('@number@', str(nScript))
    fileResult = open('./resultScript/compiledGraphFiles/serverModel/RESULT.txt', 'a+')
    fileResult.write(txtResult)
    #client side
    buttonFile = open('./resultScript/compiledGraphFiles/clientModel/button/button.txt', 'r')
    buttonText = buttonFile.read()
    buttonText = buttonText.replace('@functionName@', scriptName)
    functionFile = open('./resultScript/compiledGraphFiles/clientModel/button/function.txt', 'r')
    functionText = functionFile.read()
    functionText = functionText.replace('@functionName@', scriptName)
    linkFile = open('./resultScript/compiledGraphFiles/clientModel/button/link.txt', 'r')
    linkText = linkFile.read()
    linkText = linkText.replace('@functionName@', scriptName)
    tailResFile = open('./resultScript/compiledGraphFiles/clientModel/tailRESULT.txt', 'r')
    tailResText = tailResFile.read()
    tailResText = tailResText.replace('//@this.link@', linkText + '\n//@this.link@')
    tailResText = tailResText.replace('//@function@', functionText + '\n//@function@')
    tailResText = tailResText.replace('{/*Button*/}', buttonText +'\n{/*Button*/}')
    tailResFile.close()
    tailResFile = open('./resultScript/compiledGraphFiles/clientModel/tailRESULT.txt', 'w')
    tailResFile.write(tailResText)   






def unioneGraficiCoda():
    #server
    fileRisultato = open('./resultScript/compiledGraphFiles/serverModel/RESULT.txt', 'a')
    fileCoda = open('./resultScript/compiledGraphFiles/serverModel/tail.txt', 'r')
    letturaCoda = fileCoda.read()
    fileRisultato.write(letturaCoda)
    fileRisultato = open('./resultScript/compiledGraphFiles/serverModel/RESULT.txt', 'r')
    fileCompilato = open('./resultScript/compiledGraphFiles/compiled/antennaScript.js', 'w+')
    letturaRis = fileRisultato.read()
    fileCompilato.write(letturaRis)
    #client
    fileRisultato = open('./resultScript/compiledGraphFiles/clientModel/RESULT.txt', 'a')
    fileCoda = open('./resultScript/compiledGraphFiles/clientModel/tailRESULT.txt', 'r')
    letturaCoda = fileCoda.read()
    fileRisultato.write(letturaCoda)
    fileRisultato = open('./resultScript/compiledGraphFiles/clientModel/RESULT.txt', 'r')
    fileCompilato = open('./resultScript/compiledGraphFiles/compiled/GraphPage.js', 'w+')
    letturaRis = fileRisultato.read()
    fileCompilato.write(letturaRis)
    fileCompilato.close()
    copyfile('./resultScript/compiledGraphFiles/compiled/GraphPage.js', '../client/src/GraphPage.js')


def creazionePianificazione():
    print("ciao")
    fileProgRis = open('./programmazione.txt', 'w')
    scriptFiles = './resultScript/convertedScripts/RESULTS'
    f = []
    names = list()
    timings = list()
    variables = list()
    for (dirpath, dirnames, filenames) in walk(scriptFiles):
        f.extend(filenames)
        for items in f:
            print("Checking if in " + items + " items[" + str(len(items)-4) + "] = " + items[len(items)-4] + "is not B")
            if items[len(items)-4] is not 'B':
                names.append(str(items))
                items = items.replace('.py', '')
                items = items.split("-")
                timings.append(str(items[2]))
                print('timing = ' + items[2])
                variables.extend([0])
        break
    print(str(names))
    print(str(timings))
    print(str(variables))
    


dir = "./resultScript/convertedScripts/RESULTS"
if os.path.exists(dir):
    shutil.rmtree(dir)
os.makedirs(dir)
fileGuida = open('./resultScript/compiledGraphFiles/serverModel/base.txt', 'r')
fileRisultato = open('./resultScript/compiledGraphFiles/serverModel/RESULT.txt', 'w')
bufferLettura = fileGuida.read()
fileRisultato.write(bufferLettura)
fileGuida = open('./resultScript/compiledGraphFiles/clientModel/base.txt', 'r')
fileRisultato = open('./resultScript/compiledGraphFiles/clientModel/RESULT.txt', 'w')
bufferLettura = fileGuida.read()
fileRisultato.write(bufferLettura)
fileGuida.close()
fileRisultato.close()
fileGuida = open('./resultScript/compiledGraphFiles/clientModel/tail.txt', 'r')
fileRisultato = open('./resultScript/compiledGraphFiles/clientModel/tailRESULT.txt', 'w+')
bufferLettura = fileGuida.read()
fileRisultato.write(bufferLettura)
fileRisultato.close()
comandsFile = open("comandi.txt","r")
print('A')
comandoAttuale = ""
secondiRipetizione = ""
nScript = 0
nGraf = 0
lettura = ""
scriptName = ""
        

for line in comandsFile:
    line = line.replace('\n','')
    arrayLinea = line.split(" ")
    if arrayLinea[0] is '#':
        comandoAttuale = arrayLinea[1]
        if comandoAttuale == "condizione":
            secondiRipetizione = arrayLinea[3]
    if arrayLinea[0] is '(':
        print('Entrato nel compilatore')
        print(comandoAttuale)
        if comandoAttuale == "grafico":
            print('Dovrebbe entrare')
            compilazioneGrafico(arrayLinea, nGraf, 0)
            nGraf += 1
        if comandoAttuale == "indicatore":
            print('Dovrebbe entrare nell indicatore')
            compilazioneGrafico(arrayLinea, nGraf, 1)
            nGraf += 1
        if comandoAttuale == "condizione":
            line = line.split(" ")
            compilatoreScript(line, comandsFile, scriptName, secondiRipetizione, nScript)
            nScript += 1
        if comandoAttuale == "bottoneCondizione":
            print("oiiiiiii")
            compilatoreBottoneCondizione(arrayLinea, comandsFile, scriptName, nScript)
    if arrayLinea[0] == "NAME:":
        scriptName = arrayLinea[1]
        if comandoAttuale == "bottone":
            compilatoreBottone(comandsFile, scriptName, nScript)
    if arrayLinea[0] == "Create":   
        unioneGraficiCoda()
        print('Apposto!')  
        exit() 

                        