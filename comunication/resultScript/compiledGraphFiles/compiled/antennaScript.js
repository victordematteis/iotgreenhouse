const express = require('express');
const app = express();
const server = require('http').Server(app);
const io = require('socket.io')(server,{
    upgradeTimeout: 30000
});
const path = require('path');
const port = process.env.PORT || 3001;
const socketPort = 3002;
const bodyParser = require('body-parser');
const fs = require('fs');
const morgan = require('morgan');
const session = require('express-session');
const child = require('child_process');
const macaddress = require('macaddress');
const { execFile } = require('child_process');
const execSync = require('exec-sync');
const image2base64 = require('image-to-base64');
const formidable = require('formidable');

app.set('views', path.join(__dirname, 'view'));
app.set('view engine', 'ejs');
app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(morgan('combined'));
app.set('trust proxy', 1);
app.use(session({
	secret: 'testeriano',
	resave: false,
  	saveUninitialized: true,
	cookie:{
		originalMaxAge: 1000 * 60 * 1000 //1000 minuti
	}
}));
function startRealTimeCom(socketPort){
    const sensorsQta = 3;
    server.listen(socketPort, function(){
        console.log('listening on port ' + socketPort);
        io.on('connection', function (socket) {
            console.log("USER CONNECTED...");
        });
    });

/*@startupSequence@*/
var childPy0 = require('child_process').execFile('./comunication/comunicationScripts/letturaDati.py', [ 
    /*-->*/ 'tty-dati1', '4', '0', '0', '0' 
        ], { 
        detached: true, 
        stdio: [ 'ignore', 1, 2 ]
    }); 
    childPy0.unref(); 
    childPy0.stdout.on('data', function(data) {
    var stringaDati = data.toString();
        var arraySensori = stringaDati.split("/");
        var messaggioRicevuto = arraySensori[0];/*<---*/
        if (messaggioRicevuto != undefined){
            messaggioRicevuto = messaggioRicevuto.toString()
            var result = 0;
            while(result != -1){
                result = messaggioRicevuto.search(" ");
                messaggioRicevuto = messaggioRicevuto.replace(" ", ",");
            }
            console.log(messaggioRicevuto);
            io.emit('graphData' + /*-->*/ 'tty-dati1' + '0' /*<--*/, messaggioRicevuto);
        }    
    });
var childPy1 = require('child_process').execFile('./comunication/comunicationScripts/letturaDati.py', [ 
    /*-->*/ 'tty-dati1', '4', '0', '0', '0' 
        ], { 
        detached: true, 
        stdio: [ 'ignore', 1, 2 ]
    }); 
    childPy1.unref(); 
    childPy1.stdout.on('data', function(data) {
    var stringaDati = data.toString();
        var arraySensori = stringaDati.split("/");
        var messaggioRicevuto = arraySensori[1];/*<---*/
        if (messaggioRicevuto != undefined){
            messaggioRicevuto = messaggioRicevuto.toString()
            var result = 0;
            while(result != -1){
                result = messaggioRicevuto.search(" ");
                messaggioRicevuto = messaggioRicevuto.replace(" ", ",");
            }
            console.log(messaggioRicevuto);
            io.emit('graphData' + /*-->*/ 'tty-dati1' + '1' /*<--*/, messaggioRicevuto);
        }    
    });
var childPy2 = require('child_process').execFile('./comunication/comunicationScripts/letturaDati.py', [ 
    /*-->*/ 'tty-dati1', '4', '0', '0', '0' 
        ], { 
        detached: true, 
        stdio: [ 'ignore', 1, 2 ]
    }); 
    childPy2.unref(); 
    childPy2.stdout.on('data', function(data) {
    var stringaDati = data.toString();
        var arraySensori = stringaDati.split("/");
        var messaggioRicevuto = arraySensori[2];/*<---*/
        if (messaggioRicevuto != undefined){
            messaggioRicevuto = messaggioRicevuto.toString()
            var result = 0;
            while(result != -1){
                result = messaggioRicevuto.search(" ");
                messaggioRicevuto = messaggioRicevuto.replace(" ", ",");
            }
            console.log(messaggioRicevuto);
            io.emit('graphData' + /*-->*/ 'tty-dati1' + '2' /*<--*/, messaggioRicevuto);
        }    
    });    app.post('/cambiaDati1', function (req, res) {
        var sys = require('sys')
        var exec = require('child_process').exec;
        function puts(error, stdout, stderr) { sys.puts(stdout) }
        exec("python3 ./comunication/resultScript/convertedScripts/RESULTS/cambiaDati1-0-B.py", puts);
        res.end();
    });    app.post('/mostraDati', function (req, res) {
        var sys = require('sys')
        var exec = require('child_process').exec;
        function puts(error, stdout, stderr) { sys.puts(stdout) }
        exec("python3 ./comunication/resultScript/convertedScripts/RESULTS/mostraDati-0-B.py", puts);
        res.end();
    });}

exports.startRealTimeCom = startRealTimeCom;