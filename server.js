const express = require('express');
const app = express();
const server = require('http').Server(app);
const io = require('socket.io')(server);
const path = require('path');
const port = process.env.PORT || 3001;
const socketPort = 3002;
const bodyParser = require('body-parser');
const fs = require('fs');
const morgan = require('morgan');
const session = require('express-session');
const child = require('child_process');
const macaddress = require('macaddress');
const { execFile } = require('child_process');
const execSync = require('exec-sync');
const image2base64 = require('image-to-base64');
//const script = require("./script/updateData.js");
const formidable = require('formidable');


var initiated = 1;
if (initiated){
    initiated = 0;
	/*> TO UNCOMENT >*///child.exec('sudo python ./script/letturaDati.py tty-dati1 4 -act &');
	/*letturaDati.py 
		[1: device name] 
		[2: number of sensors attached]    
		[3: '-act' if instructions have to be performed every read cicle (when data is read, all comands will be forgotten!!)] 
		[4: '-start' to create void file of instructions.txt] 
		[5: number of instructions] */
	/*> TO UNCOMENT  >*///child.exec('sudo python ./script/attuatoreGenerale.py tty-irrigazione 0 0 0 -start 3 &');
	/*attuatoreGenerale.py 
		[1: device name] 
		[2: value to change]
		[3: new value] 
		[4: time to wait] 
		[5: '-start' create void instructions document] 
		[6: number of of instructions]*/
		child.exec('sudo modprobe bcm2835-v4l2');
		//child.exec('cd client && npm start');
}


// setting up views
app.set('views', path.join(__dirname, 'view'));
app.set('view engine', 'ejs');
app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(morgan('combined'));
app.set('trust proxy', 1);
app.use(session({
	secret: 'testeriano',
	resave: false,
  	saveUninitialized: true,
	cookie:{
		originalMaxAge: 1000 * 60 * 1000 //1000 minuti
	}
}));

/*
var frameDataBufferizzato;
const raspberryPiCamera = require('raspberry-pi-camera-native');

raspberryPiCamera.on('frame', (frameData) => {
	frameDataBufferizzato = new Buffer(frameData).toString('base64');
});

var test = raspberryPiCamera.start({
	width: 640,
	height: 360,
	fps: 30,
	encoding: 'JPEG',
	quality: 10
});
*/

var sessionList = [], sessionBanned = [], i = 0, k = 0;
var cpuUsage = 0;



const scriptAntenna = require('./comunication/resultScript/compiledGraphFiles/compiled/antennaScript.js');


setInterval(() => {
	scriptAntenna.startRealTimeCom(socketPort);
}, 60000);






